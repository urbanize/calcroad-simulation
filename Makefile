.PHONY: dist

dist:
	rm -rf dist/*
	python3 setup.py sdist bdist_wheel
	python3 -m twine upload dist/*

test:
	python3 test.py
