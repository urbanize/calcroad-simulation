
class SimulationError(Exception):
    pass


class PathNotFound(SimulationError):
    pass
