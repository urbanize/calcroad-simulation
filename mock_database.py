from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine


class Database:
    def __init__(self, database_uri):
        self.engine = create_engine(database_uri)
        self.session = sessionmaker(bind=self.engine)()
        self.Model = declarative_base(bind=self.engine)
        
    def create_all(self):
        self.Model.metadata.create_all()
    
    def drop_all(self):
        self.Model.metadata.drop_all()