import json
from calcroad_simulation import Simulation, SimulationStream, SimulatorSettings
from mock_database import Database

db = Database('sqlite:///:memory:')
from sqlalchemy import Column, Integer, String, Float

class VehiculePosition(db.Model):
    __tablename__ = 'vehicule_position'
    id = Column(String, primary_key=True)
    map_id = Column(Integer, primary_key=True)
    time = Column(Integer, primary_key=True)
    
    speed = Column(Integer, default=50)
    lat = Column(Float, nullable=False)
    lng = Column(Float, nullable=False)
    
    def __str__(self):
        return f"[{self.time}]\t(Vehicule {self.id}) {self.lat} {self.lng}"


db.create_all()

def test_simulation():
    roads = json.load(open("test_data/roads.json"))
    journeys = json.load(open("test_data/journeys.json"))
    incidents = json.load(open("test_data/incidents.json"))
    
    SimulatorSettings.from_dict({
        'db_session': db.session,
        'position_model': VehiculePosition
    })
    

    simulation = Simulation(
        map_id=1,
        roads=roads,
        incidents=incidents,
        journeys=journeys,
        debug=True
    )

    simulation.run()
    
    geojson = SimulationStream.dump(
        map_id=1, 
        start_time=290, 
        limit=30
    )

    with open('output.json', 'w') as output:
        json.dump(geojson, output, indent=4)


if __name__ == '__main__':
    test_simulation()
